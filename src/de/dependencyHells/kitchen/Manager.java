package de.dependencyHells.kitchen;

import java.util.List;

import de.dependencyHells.kitchen.api.Ingredient;
import de.dependencyHells.kitchen.tools.Cupboard;
import de.dependencyHells.kitchen.tools.Fridge;

public class Manager {

	private final Fridge fridge;
	private final Cupboard cupboard;

	public Manager(Fridge fridge, Cupboard cupboard) {
		super();
		this.fridge = fridge;
		this.cupboard = cupboard;
	}

	/**
	 * Nimmt eine Lieferung von Zutaten entgegen.
	 * 
	 * @param ingredients Zutaten
	 */
	public void acceptDelivery(List<Ingredient> ingredients) {
		for (Ingredient i : ingredients) {
			if (i.storeInFridge()) {
				fridge.put(i);
			} else {
				cupboard.put(i);
			}
		}
	}
}
