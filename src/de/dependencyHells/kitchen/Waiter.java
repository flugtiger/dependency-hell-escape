package de.dependencyHells.kitchen;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import de.dependencyHells.kitchen.api.Ingredient;
import de.dependencyHells.kitchen.api.MealType;
import de.dependencyHells.kitchen.api.Receipt;

public class Waiter {

	private final Scanner scanner;
	private final Map<MealType, List<Receipt>> mealTypeToReceipts;

	public Waiter(Scanner scanner, List<Receipt> receipts) {
		super();
		this.scanner = scanner;
		this.mealTypeToReceipts = receipts.stream().collect(Collectors.groupingBy(Receipt::getType));
	}

	/**
	 * Lässt den Benutzer ein Gericht auswählen.
	 * 
	 * @param type    Typ, aus dem der Benutzer ein Gericht wählen soll
	 * @param forMeal übergeordnetes Gericht oder null
	 * @return gewähltes Rezept
	 */
	public Receipt letUserChooseMeal(MealType type, String forMeal) {
		List<Receipt> receipts = mealTypeToReceipts.get(type);
		if (receipts.size() == 1) {
			return receipts.get(0);
		}
		List<String> mealNames = receipts.stream().map(Receipt::getMealName).collect(toList());
		int choice = getChoice(
				"What " + type.name().toLowerCase() + " do you want"
						+ (forMeal == null ? "?" : " to your " + forMeal + "?"),
				mealNames,
				"");
		Receipt receipt = receipts.get(choice - 1);
		return receipt;
	}

	public void deliverMeal(MealType type, Ingredient meal) {
		System.out.println("\n=== Enjoy your " + type + ": ===");
		System.out.println(meal + "\n");
	}

	public void close() {
		scanner.close();
	}

	/**
	 * Stellt dem Benutzer eine Auswahl-Frage.
	 * 
	 * @param pre     Beginn der Frage
	 * @param choices Wahlmöglichkeiten
	 * @param post    Ende der Frage
	 * @return index der Auswahl
	 */
	private int getChoice(String pre, List<String> choices, String post) {
		System.out.println(pre);
		for (int i = 1; i <= choices.size(); i++) {
			System.out.println(String.format("\t(%d) %s", i, choices.get(i - 1)));
		}
		System.out.println(post);

		int choice = 0;
		while (choice < 1 || choice > choices.size()) {
			try {
				choice = Integer.parseInt(scanner.nextLine());
			} catch (NumberFormatException e) {
				System.out.println(e.getMessage());
			}
			if (choice < 1 || choice > choices.size()) {
				System.out.println("I did not understand, try again:");
			}
		}
		return choice;
	}
}
