package de.dependencyHells.kitchen;

import java.util.List;
import java.util.stream.Stream;

import de.dependencyHells.kitchen.api.Ingredient;
import de.dependencyHells.kitchen.api.MealType;
import de.dependencyHells.kitchen.api.Receipt;

public class MainHelper {

	private final List<Ingredient> ingredients;
	private final Manager manager;
	private final Waiter waiter;
	private final Cook cook;

	public MainHelper(List<Ingredient> ingredients, Manager manager, Waiter waiter, Cook cook) {
		super();
		this.ingredients = ingredients;
		this.manager = manager;
		this.waiter = waiter;
		this.cook = cook;
	}

	public void execute() {
		manager.acceptDelivery(ingredients);
		Stream.of(MealType.MAIN_MEAL, MealType.DESERT).forEach(type -> {
			Receipt receipt = waiter.letUserChooseMeal(type, null);
			Ingredient meal = cook.prepareMealWithDependencies(receipt);
			waiter.deliverMeal(type, meal);
		});
		waiter.close();
	}
}
