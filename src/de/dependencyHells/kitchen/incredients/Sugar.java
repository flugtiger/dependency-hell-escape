package de.dependencyHells.kitchen.incredients;

import de.dependencyHells.kitchen.api.Ingredient;

public class Sugar implements Ingredient {

	@Override
	public String toString() {
		return "sugar";
	}

	@Override
	public boolean storeInFridge() {
		return false;
	}

}
