package de.dependencyHells.kitchen.incredients;

import de.dependencyHells.kitchen.api.Ingredient;

public class Tomatoes implements Ingredient {
	@Override
	public String toString() {
		return "tomatoes";
	}

	@Override
	public boolean storeInFridge() {
		return false;
	}
}
