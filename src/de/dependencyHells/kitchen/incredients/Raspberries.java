package de.dependencyHells.kitchen.incredients;

import de.dependencyHells.kitchen.api.Ingredient;

public class Raspberries implements Ingredient {

	@Override
	public String toString() {
		return "raspberries";
	}

	@Override
	public boolean storeInFridge() {
		return false;
	}
}
