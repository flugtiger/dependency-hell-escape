package de.dependencyHells.kitchen.incredients;

import de.dependencyHells.kitchen.api.Ingredient;

public class Strawberries implements Ingredient {

	@Override
	public String toString() {
		return "strawberries";
	}

	@Override
	public boolean storeInFridge() {
		return true;
	}
}
