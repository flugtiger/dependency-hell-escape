package de.dependencyHells.kitchen.incredients;

import de.dependencyHells.kitchen.api.Ingredient;

public class Spices implements Ingredient {

	@Override
	public String toString() {
		return "spices";
	}

	@Override
	public boolean storeInFridge() {
		return false;
	}
}
