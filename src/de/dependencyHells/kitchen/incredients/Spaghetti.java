package de.dependencyHells.kitchen.incredients;

import de.dependencyHells.kitchen.api.Ingredient;

public class Spaghetti implements Ingredient {

	@Override
	public String toString() {
		return "spaghetti";
	}

	@Override
	public boolean storeInFridge() {
		return false;
	}
}
