package de.dependencyHells.kitchen.incredients;

import de.dependencyHells.kitchen.api.Ingredient;

public class Cheese implements Ingredient {

	@Override
	public String toString() {
		return "cheese";
	}

	@Override
	public boolean storeInFridge() {
		return true;
	}
}
