package de.dependencyHells.kitchen.incredients;

import de.dependencyHells.kitchen.api.Ingredient;

public class Cream implements Ingredient {

	@Override
	public String toString() {
		return "cream";
	}

	@Override
	public boolean storeInFridge() {
		return true;
	}
}
