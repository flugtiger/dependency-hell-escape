package de.dependencyHells.kitchen.incredients;

import de.dependencyHells.kitchen.api.Ingredient;

public class Butter implements Ingredient {

	@Override
	public String toString() {
		return "butter";
	}

	@Override
	public boolean storeInFridge() {
		return true;
	}

}
