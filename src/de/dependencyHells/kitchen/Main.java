package de.dependencyHells.kitchen;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import de.dependencyHells.kitchen.api.Ingredient;
import de.dependencyHells.kitchen.incredients.Butter;
import de.dependencyHells.kitchen.incredients.Cheese;
import de.dependencyHells.kitchen.incredients.Cream;
import de.dependencyHells.kitchen.incredients.Raspberries;
import de.dependencyHells.kitchen.incredients.Spaghetti;
import de.dependencyHells.kitchen.incredients.Spices;
import de.dependencyHells.kitchen.incredients.Strawberries;
import de.dependencyHells.kitchen.incredients.Sugar;
import de.dependencyHells.kitchen.incredients.Tomatoes;
import de.dependencyHells.kitchen.tools.Cupboard;
import de.dependencyHells.kitchen.tools.Freezer;
import de.dependencyHells.kitchen.tools.Fridge;
import de.dependencyHells.kitchen.tools.Grater;
import de.dependencyHells.kitchen.tools.ImmersionBlender;
import de.dependencyHells.kitchen.tools.Stove;

public class Main {

	public static void main(String[] args) {
		List<Ingredient> ingredients = Arrays.asList(
				new Butter(), new Cheese(), new Cream(), new Raspberries(), new Spaghetti(),
				new Spices(), new Strawberries(), new Sugar(), new Tomatoes());

		Fridge fridge = new Fridge();
		Cupboard cupboard = new Cupboard();

		Manager manager = new Manager(fridge, cupboard);

		Kitchen kitchen = new Kitchen(
				fridge, cupboard, new Stove(), new ImmersionBlender(),
				new Grater(), new Freezer());

		Waiter waiter = new Waiter(new Scanner(System.in), kitchen.getReceipts());

		Cook cook = new Cook(waiter);

		MainHelper helper = new MainHelper(ingredients, manager, waiter, cook);
		helper.execute();
	}
}
