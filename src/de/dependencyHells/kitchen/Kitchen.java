package de.dependencyHells.kitchen;

import java.util.ArrayList;
import java.util.List;

import de.dependencyHells.kitchen.api.Receipt;
import de.dependencyHells.kitchen.receipts.CheeseSauceReceipt;
import de.dependencyHells.kitchen.receipts.RaspberryReceipt;
import de.dependencyHells.kitchen.receipts.SorbetReceipt;
import de.dependencyHells.kitchen.receipts.SpaghettiReceipt;
import de.dependencyHells.kitchen.receipts.StrawberryReceipt;
import de.dependencyHells.kitchen.receipts.TomatoSauceReceipt;
import de.dependencyHells.kitchen.tools.Cupboard;
import de.dependencyHells.kitchen.tools.Freezer;
import de.dependencyHells.kitchen.tools.Fridge;
import de.dependencyHells.kitchen.tools.Grater;
import de.dependencyHells.kitchen.tools.ImmersionBlender;
import de.dependencyHells.kitchen.tools.Stove;

public class Kitchen {

	private final Fridge fridge;
	private final Cupboard cupboard;
	private final Stove stove;
	private final ImmersionBlender blender;
	private final Grater grater;
	private final Freezer freezer;

	public Kitchen(Fridge fridge, Cupboard cupboard, Stove stove, ImmersionBlender blender, Grater grater,
			Freezer freezer) {
		super();
		this.fridge = fridge;
		this.cupboard = cupboard;
		this.stove = stove;
		this.blender = blender;
		this.grater = grater;
		this.freezer = freezer;
	}

	public List<Receipt> getReceipts() {
		List<Receipt> result = new ArrayList<>();
		result.add(new TomatoSauceReceipt(cupboard, blender, fridge, stove));
		result.add(new CheeseSauceReceipt(fridge, grater, stove));
		result.add(new SpaghettiReceipt(cupboard, stove));
		result.add(new RaspberryReceipt(cupboard));
		result.add(new StrawberryReceipt(fridge));
		result.add(new SorbetReceipt(blender, cupboard, freezer));
		return result;
	}
}
