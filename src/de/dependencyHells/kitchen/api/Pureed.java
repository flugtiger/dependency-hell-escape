package de.dependencyHells.kitchen.api;

public class Pureed<T extends Ingredient> implements Ingredient {

	private final T ingredient;

	public Pureed(T ingredient) {
		this.ingredient = ingredient;
	}

	@Override
	public String toString() {
		return "pureed " + ingredient;
	}

	@Override
	public boolean storeInFridge() {
		return false;
	}
}
