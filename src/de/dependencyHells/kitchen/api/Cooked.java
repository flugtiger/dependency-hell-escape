package de.dependencyHells.kitchen.api;

public class Cooked<T extends Ingredient> implements Ingredient {

	private final T ingredient;

	public Cooked(T ingredient) {
		this.ingredient = ingredient;
	}

	@Override
	public String toString() {
		return "cooked " + ingredient;
	}

	@Override
	public boolean storeInFridge() {
		return false;
	}
}
