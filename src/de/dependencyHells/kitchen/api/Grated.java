package de.dependencyHells.kitchen.api;

public class Grated implements Ingredient {

	private final Ingredient ingredient;

	public Grated(Ingredient ingredient) {
		this.ingredient = ingredient;
	}

	@Override
	public String toString() {
		return "grated " + ingredient;
	}

	@Override
	public boolean storeInFridge() {
		return false;
	}
}
