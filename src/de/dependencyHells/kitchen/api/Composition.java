package de.dependencyHells.kitchen.api;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Composition implements Ingredient {

	private final Ingredient[] ingredients;

	public Composition(Ingredient... ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public String toString() {
		return Stream.of(ingredients)
				.map(Ingredient::toString)
				.collect(Collectors.joining(" and ", "(", ")"));
	}

	@Override
	public boolean storeInFridge() {
		return false;
	}
}
