package de.dependencyHells.kitchen.api;

import java.util.List;

public interface Receipt {

	MealType getType();

	String getMealName();

	Ingredient prepareMeal(Ingredient... input);

	List<MealType> getInputMealTypes();
}
