package de.dependencyHells.kitchen.api;

public enum MealType {

	SAUCE,

	MAIN_MEAL,

	FRUIT,

	DESERT;

}
