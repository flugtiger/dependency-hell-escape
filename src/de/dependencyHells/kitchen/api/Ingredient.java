package de.dependencyHells.kitchen.api;

public interface Ingredient {

	/**
	 * @return true, falls die Zutat in den Kühlschrank gehört
	 */
	boolean storeInFridge();
}
