package de.dependencyHells.kitchen.api;

public class Freezed implements Ingredient {
	private final Ingredient ingredient;

	public Freezed(Ingredient ingredient) {
		this.ingredient = ingredient;
	}

	@Override
	public String toString() {
		return "freezed " + ingredient;
	}

	@Override
	public boolean storeInFridge() {
		return true;
	}
}
