package de.dependencyHells.kitchen.tools;

import java.util.HashMap;

import de.dependencyHells.kitchen.api.Ingredient;

/**
 * Abstrakte Klasse zur Aufbewahrung von Zutaten.
 */
public abstract class IngredientStorage extends HashMap<String, Ingredient> {
	private static final long serialVersionUID = 1L;
	
	private final String name;
	
	/**
	 * @param name Name
	 */
	public IngredientStorage(String name) {
		this.name = name;
	}
	
	public void put(Ingredient ingredient) {
		put(ingredient.toString(), ingredient);
	}
	
	@Override
	public Ingredient get(Object key) {
		System.out.println("getting '" + key + "' from " + name);
		if(!containsKey(key)) {
			throw new RuntimeException(name + " does not contain requested item '" + key + "'");
		}
		return super.get(key);
	}
}
