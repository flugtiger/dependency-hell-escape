package de.dependencyHells.kitchen.tools;

import de.dependencyHells.kitchen.api.Freezed;
import de.dependencyHells.kitchen.api.Ingredient;

public class Freezer {

	/**
	 * Friert die Zutat ein.
	 * 
	 * @param ingredient einzufrierende Zutat
	 * @return gefrorene Zutat
	 */
	public Ingredient freeze(Ingredient ingredient) {
		System.out.println("freezing '" + ingredient + "'");
		return new Freezed(ingredient);
	}
	
}
