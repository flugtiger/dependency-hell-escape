package de.dependencyHells.kitchen.tools;

import de.dependencyHells.kitchen.api.Ingredient;
import de.dependencyHells.kitchen.api.Pureed;

public class ImmersionBlender {

	/**
	 * Püriert die übergebene Zutat.
	 * 
	 * @param ingredient Zutat
	 * @return pürierte Zutat
	 */
	public <T extends Ingredient> Pureed<T> blend(T ingredient) {
		System.out.println("blending '" + ingredient + "'");
		return new Pureed<>(ingredient);
	}
}
