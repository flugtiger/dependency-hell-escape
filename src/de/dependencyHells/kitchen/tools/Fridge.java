package de.dependencyHells.kitchen.tools;

/**
 * Kühlschrank.
 */
public class Fridge extends IngredientStorage {
	private static final long serialVersionUID = 1L;
	
	public Fridge() {
		super("fridge");
	}
}
