package de.dependencyHells.kitchen.tools;

import de.dependencyHells.kitchen.api.Cooked;
import de.dependencyHells.kitchen.api.Ingredient;

public class Stove {

	public Ingredient cook(Ingredient ingredient) {
		System.out.println("cooking '" + ingredient + "'");
		return new Cooked<>(ingredient);
	}
}
