package de.dependencyHells.kitchen.tools;

import de.dependencyHells.kitchen.api.Grated;
import de.dependencyHells.kitchen.api.Ingredient;

public class Grater {

	public Ingredient grate(Ingredient ingredient) {
		System.out.println("grating " + ingredient);
		return new Grated(ingredient);
	}
	
}
