package de.dependencyHells.kitchen.tools;

/**
 * Schrank.
 */
public class Cupboard extends IngredientStorage {
	private static final long serialVersionUID = 1L;

	public Cupboard() {
		super("cupboard");
	}
}
