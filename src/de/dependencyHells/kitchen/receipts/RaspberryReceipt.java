package de.dependencyHells.kitchen.receipts;

import static java.util.Collections.emptyList;

import java.util.List;

import de.dependencyHells.kitchen.api.Ingredient;
import de.dependencyHells.kitchen.api.MealType;
import de.dependencyHells.kitchen.api.Receipt;
import de.dependencyHells.kitchen.tools.Cupboard;

public class RaspberryReceipt implements Receipt {

	private final Cupboard cupboard;

	public RaspberryReceipt(Cupboard cupboard) {
		super();
		this.cupboard = cupboard;
	}

	@Override
	public MealType getType() {
		return MealType.FRUIT;
	}

	@Override
	public String getMealName() {
		return "raspberries";
	}

	@Override
	public Ingredient prepareMeal(Ingredient... input) {
		return cupboard.get("raspberries");
	}

	@Override
	public List<MealType> getInputMealTypes() {
		return emptyList();
	}

}
