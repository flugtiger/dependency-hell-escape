package de.dependencyHells.kitchen.receipts;

import static java.util.Collections.emptyList;

import java.util.List;

import de.dependencyHells.kitchen.api.Ingredient;
import de.dependencyHells.kitchen.api.MealType;
import de.dependencyHells.kitchen.api.Receipt;
import de.dependencyHells.kitchen.tools.Fridge;

public class StrawberryReceipt implements Receipt {

	private final Fridge fridge;

	public StrawberryReceipt(Fridge fridge) {
		this.fridge = fridge;
	}

	@Override
	public MealType getType() {
		return MealType.FRUIT;
	}

	@Override
	public String getMealName() {
		return "strawberries";
	}

	@Override
	public Ingredient prepareMeal(Ingredient... input) {
		return fridge.get("strawberries");
	}

	@Override
	public List<MealType> getInputMealTypes() {
		return emptyList();
	}

}
