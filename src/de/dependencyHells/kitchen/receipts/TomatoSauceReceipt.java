package de.dependencyHells.kitchen.receipts;

import static java.util.Collections.emptyList;

import java.util.List;

import de.dependencyHells.kitchen.api.Composition;
import de.dependencyHells.kitchen.api.Ingredient;
import de.dependencyHells.kitchen.api.MealType;
import de.dependencyHells.kitchen.api.Receipt;
import de.dependencyHells.kitchen.tools.Cupboard;
import de.dependencyHells.kitchen.tools.Fridge;
import de.dependencyHells.kitchen.tools.ImmersionBlender;
import de.dependencyHells.kitchen.tools.Stove;

public class TomatoSauceReceipt implements Receipt {

	private Cupboard cupboard;
	private ImmersionBlender blender;
	private Fridge fridge;
	private Stove stove;

	public TomatoSauceReceipt(Cupboard cupboard, ImmersionBlender blender, Fridge fridge, Stove stove) {
		super();
		this.cupboard = cupboard;
		this.blender = blender;
		this.fridge = fridge;
		this.stove = stove;
	}

	@Override
	public Ingredient prepareMeal(Ingredient... input) {
		Ingredient tomatoes = cupboard.get("tomatoes");
		Ingredient blendedTomatoes = blender.blend(tomatoes);
		Ingredient rawSauce = new Composition(blendedTomatoes, fridge.get("cream"));
		Ingredient cookedSauce = stove.cook(rawSauce);
		return cookedSauce;
	}

	@Override
	public MealType getType() {
		return MealType.SAUCE;
	}

	@Override
	public String getMealName() {
		return "tomato sauce";
	}

	@Override
	public List<MealType> getInputMealTypes() {
		return emptyList();
	}
}
