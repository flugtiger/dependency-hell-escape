package de.dependencyHells.kitchen.receipts;

import java.util.Arrays;
import java.util.List;

import de.dependencyHells.kitchen.api.Composition;
import de.dependencyHells.kitchen.api.Ingredient;
import de.dependencyHells.kitchen.api.MealType;
import de.dependencyHells.kitchen.api.Receipt;
import de.dependencyHells.kitchen.tools.Cupboard;
import de.dependencyHells.kitchen.tools.Freezer;
import de.dependencyHells.kitchen.tools.ImmersionBlender;

public class SorbetReceipt implements Receipt {

	private final ImmersionBlender blender;
	private final Cupboard cupboard;
	private final Freezer freezer;

	public SorbetReceipt(ImmersionBlender blender, Cupboard cupboard, Freezer freezer) {
		this.blender = blender;
		this.cupboard = cupboard;
		this.freezer = freezer;
	}

	@Override
	public Ingredient prepareMeal(Ingredient... berries) {
		Ingredient blendedBerries = blender.blend(berries[0]);
		Ingredient sweetBerries = new Composition(blendedBerries, cupboard.get("sugar"));
		Ingredient sorbet = freezer.freeze(sweetBerries);
		return sorbet;
	}

	@Override
	public MealType getType() {
		return MealType.DESERT;
	}

	@Override
	public String getMealName() {
		return "sorbet";
	}

	@Override
	public List<MealType> getInputMealTypes() {
		return Arrays.asList(MealType.FRUIT);
	}
}
