package de.dependencyHells.kitchen.receipts;

import java.util.Arrays;
import java.util.List;

import de.dependencyHells.kitchen.api.Composition;
import de.dependencyHells.kitchen.api.Ingredient;
import de.dependencyHells.kitchen.api.MealType;
import de.dependencyHells.kitchen.api.Receipt;
import de.dependencyHells.kitchen.tools.Cupboard;
import de.dependencyHells.kitchen.tools.Stove;

public class SpaghettiReceipt implements Receipt {

	private Cupboard cupboard;
	private Stove stove;

	public SpaghettiReceipt(Cupboard cupboard, Stove stove) {
		super();
		this.cupboard = cupboard;
		this.stove = stove;
	}

	@Override
	public Ingredient prepareMeal(Ingredient... sauce) {
		Ingredient rawSpaghetti = cupboard.get("spaghetti");
		Ingredient cookedSpaghetti = stove.cook(rawSpaghetti);

		Ingredient result = new Composition(cookedSpaghetti, sauce[0]);
		return result;
	}

	@Override
	public MealType getType() {
		return MealType.MAIN_MEAL;
	}

	@Override
	public String getMealName() {
		return "spaghetti";
	}

	@Override
	public List<MealType> getInputMealTypes() {
		return Arrays.asList(MealType.SAUCE);
	}
}
