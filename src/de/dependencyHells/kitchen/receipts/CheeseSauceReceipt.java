package de.dependencyHells.kitchen.receipts;

import static java.util.Collections.emptyList;

import java.util.List;

import de.dependencyHells.kitchen.api.Composition;
import de.dependencyHells.kitchen.api.Ingredient;
import de.dependencyHells.kitchen.api.MealType;
import de.dependencyHells.kitchen.api.Receipt;
import de.dependencyHells.kitchen.tools.Fridge;
import de.dependencyHells.kitchen.tools.Grater;
import de.dependencyHells.kitchen.tools.Stove;

public class CheeseSauceReceipt implements Receipt {

	private Fridge fridge;
	private Grater grater;
	private Stove stove;

	public CheeseSauceReceipt(Fridge fridge, Grater grater, Stove stove) {
		super();
		this.fridge = fridge;
		this.grater = grater;
		this.stove = stove;
	}

	@Override
	public Ingredient prepareMeal(Ingredient... input) {
		Ingredient cheese = fridge.get("cheese");
		Ingredient gratedCheese = grater.grate(cheese);
		Ingredient cream = fridge.get("cream");
		Ingredient rawSauce = new Composition(gratedCheese, cream);
		Ingredient cookedSauce = stove.cook(rawSauce);
		return cookedSauce;
	}

	@Override
	public MealType getType() {
		return MealType.SAUCE;
	}

	@Override
	public String getMealName() {
		return "cheese sauce";
	}

	@Override
	public List<MealType> getInputMealTypes() {
		return emptyList();
	}
}
