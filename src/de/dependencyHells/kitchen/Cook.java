package de.dependencyHells.kitchen;

import java.util.ArrayList;
import java.util.List;

import de.dependencyHells.kitchen.api.Ingredient;
import de.dependencyHells.kitchen.api.MealType;
import de.dependencyHells.kitchen.api.Receipt;

public class Cook {

	private final Waiter waiter;

	public Cook(Waiter waiter) {
		super();
		this.waiter = waiter;
	}

	public Ingredient prepareMealWithDependencies(Receipt receipt) {
		List<Ingredient> inputIngredients = new ArrayList<>();
		for (MealType type : receipt.getInputMealTypes()) {
			Receipt inputReceipt = waiter.letUserChooseMeal(type, receipt.getMealName());
			inputIngredients.add(prepareMealWithDependencies(inputReceipt));
		}
		Ingredient meal = receipt.prepareMeal(inputIngredients.toArray(new Ingredient[0]));
		return meal;
	}

}
